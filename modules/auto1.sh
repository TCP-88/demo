#!/bin/bash

sudo apt update -y && sudo apt upgrade -y


download (){
wget https://download.docker.com/linux/ubuntu/dists/focal/pool/stable/amd64/containerd.io_1.4.8-1_amd64.deb  
wget https://download.docker.com/linux/ubuntu/dists/focal/pool/stable/amd64/docker-ce-cli_20.10.7~3-0~ubuntu-focal_amd64.deb  
wget https://download.docker.com/linux/ubuntu/dists/focal/pool/stable/amd64/docker-ce_20.10.7~3-0~ubuntu-focal_amd64.deb 
}

file () {
mkdir /tmp/html && touch /tmp/html/index.html && echo "<h1> NAME </h>" > /tmp/html/index.html
}

run () {
sudo docker run --name demo-nginx -d -p 80:80 -v /tmp/html:/usr/share/nginx/html nginx:1.18.0
}

download && sudo dpkg -i ./*.deb && sleep 60 && sudo systemctl enable docker && sleep 60 && sudo systemctl start docker && file && run
