provider "aws" {
  region                  = "us-east-1"
  shared_credentials_file = "/home/tcp/.aws/credentials"
  profile                 = "default"
}