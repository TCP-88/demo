module "vpc" {
  source = "terraform-aws-modules/vpc/aws"
  version = "3.2.0"
  name = "demo"
  cidr = "10.0.0.0/16"

  azs             = ["us-east-1a"]
  private_subnets = ["10.0.1.0/24"]
  public_subnets  = ["10.0.2.0/24"]

  enable_nat_gateway = true

  tags = {
    Terraform   = "true"
    Environment = "demo"
  }
}

module "security-group-1" {
  source = "terraform-aws-modules/security-group/aws"
  version = "4.3.0"
  name        = "http-80"
  description = "Security group for http 80"
  vpc_id      = "${module.vpc.vpc_id}"

  ingress_cidr_blocks      = ["118.189.0.0/16","116.206.0.0/16","223.25.0.0/16"]
  ingress_rules            = ["http-80-tcp"]
  ingress_with_cidr_blocks = [
    {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      cidr_blocks = "0.0.0.0/0"
    },
  ]
  egress_rules    =["all-all"]
}

resource "tls_private_key" "this" {
  algorithm = "RSA"
}

module "key_pair" {
  source = "terraform-aws-modules/key-pair/aws"

  key_name   = "demo-1"
  public_key = tls_private_key.this.public_key_openssh
}

module "ec2-instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "2.19.0"
  name    = "demo-ubuntu"
  instance_count = 1
  ami     ="ami-09e67e426f25ce0d7"
  instance_type = "t2.micro"
  key_name  = "demo-1"
  vpc_security_group_ids = ["${module.security-group-1.security_group_id}"]
  subnet_id = "subnet-0dab25b98a13e872b"
  user_data = "${file("auto1.sh")}"

  tags = {
    name = "demo"
  }
}

module "nlb" {
  source  = "terraform-aws-modules/alb/aws"
  version = "~> 6.0"
  name = "demo-nlb"
  load_balancer_type = "network"
  vpc_id = "${module.vpc.vpc_id}"
  subnets = ["subnet-0903ab3a53b940604"]
  http_tcp_listeners = [
    {
      port               = 22
      protocol           = "TCP"
      target_group_index = 0
    },
    {
      port               = 80
      protocol           = "TCP"
      target_group_index = 1
    }
  ]
  target_groups = [
    {
      name_prefix = "ssh-1"
      backend_protocol  = "TCP"
      backend_port  = "22"
      target_type   = "instance"
      targets = {
        my_ec2 = {
          target_id = "i-0d8a9ac1ff8acf011"
          port      = 22
        }
      }
    },
    {
      name_prefix = "web-1"
      backend_protocol  = "TCP"
      backend_port  = "80"
      target_type   = "instance"
      targets = {
        my_ec2 = {
          target_id = "i-0d8a9ac1ff8acf011"
          port      = 80
        }
      }
    }
  ]
}

resource "aws_iam_user" "user" {
  name = "demotcp"
  path = "/"
  tags = {
    tag-key = "demo-only"
  }
}
  resource "aws_iam_user_policy_attachment" "attach" {
  user       = aws_iam_user.user.name
  policy_arn = "arn:aws:iam::aws:policy/ReadOnlyAccess"
}
